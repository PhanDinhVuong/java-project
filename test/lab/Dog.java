package lab;


public class Dog {
	protected String color;
	protected String name;
	protected String breed;
	protected void vayduoi(String vayduoiAction) {
		System.out.println("Vay duoi kieu: "+vayduoiAction);
	}
	protected void sua(String suaAction) {
		System.out.println("Sua kieu: "+suaAction);
	}
	protected void an(String anAction) {
		System.out.println("An kieu: "+ anAction);
	}
	protected Dog(String color, String name, String breed) {
		super();
		this.color = color;
		this.name = name;
		this.breed = breed;
	}
	public static void main(String[] args) {
		Dog dog = new Dog("do","Vu","cai");
		System.out.println("con cho 1: "+dog.name+dog.color+dog.breed);
		dog.vayduoi("a");
	}
	}
