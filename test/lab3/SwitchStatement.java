package lab3;

public class SwitchStatement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char grade = 'C';
		
		switch(grade) {
		case 'A' :
			System.out.println("Exellent!");
			break;
		case 'B' :
		case 'C' :
			System.out.println("Well done!");
			break;
		case 'D' :	
			System.out.println("You passed!");
		case 'F' :	
			System.out.println("Better Try Again!");
			break;
		default:
			System.out.println("Invalid grade");
			
		}
		System.out.println("Your grade is: "+ grade);
	}

}
